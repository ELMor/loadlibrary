
#include <stdio.h>
#include <windows.h>
#include <conio.h>

int main(int argc, char *argv[]){
	HINSTANCE hins;
	if(argc!=2){
		puts("llib fname");
		return 0;
	}
	printf("Intentando cargar %s.\n",argv[1]);
	hins=LoadLibraryEx(argv[1],NULL,DONT_RESOLVE_DLL_REFERENCES);
	if(hins){
		printf("%s cargada! Pulse una tecla para descargarla\n",argv[1]);
		getch();
		FreeLibrary(hins);
	}else{
		printf("Fallo la carga de %s",argv[1]);
	}
	return 0;
}